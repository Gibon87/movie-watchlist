import React from 'react';
import {useState} from 'react';
import {ResultCard} from "./ResultCard";
import {Error} from "./Error";

const API_KEY="85eb6bf2b9b39ba0dd7d348d80493514";


export const Add = () => {
    const [query, setQuery] = useState("");
    const [results, setResult] = useState([]);

    const handleChange = (e) => {
        e.preventDefault();
        setQuery(e.target.value);
        console.log(query);
    };

    const handleClick = () => {
        fetch(
            `https://api.themoviedb.org/3/search/movie?api_key=${API_KEY}&language=en-US&page=1&include_adult=false&query=${query}`
        )
            .then((res) => res.json())
            .then((data) => {
                if(!data.errors){
                    console.log(data);
                    setResult(data.results);
                }
                else {
                    setResult([]);
                    console.log("404");
                }
            });
    };

    return (
        <div className="add">
            <div className="container">
               <div className="add-content">
                   <div className="input-wrapper flex">
                       <input type="text"
                              placeholder={"search for a movie"}
                              value={query}
                              onKeyUp={e => {
                                  if (e.keyCode == 13) {
                                      handleClick();
                                  }
                              }}
                              onChange={handleChange}
                       />
                       <button
                           className={"btn btn-margin"}
                           onClick={handleClick}>Search</button>
                   </div>
                   {results.length > 0 && (
                       <ul className="results">
                           {results.map((movie) => (
                               <li key={movie.id}>
                                   <ResultCard movie={movie}/>
                               </li>
                           ))}
                       </ul>
                   )}
               </div>
            </div>
        </div>
    )
}